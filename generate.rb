require "csv"
require "anki2"

anki = Anki2.new({
  name: "Ruby Chunks",
  output_path: 'ruby_chunks.apkg'
})

counter = 0

CSV.foreach("ruby_chunks.tsv", col_sep: "\t") do |row|
  if row.size != 2
    raise "Row should be a tab-separated Front\tBack pair"
  else
    anki.add_card(*row)
    counter += 1
  end
end

anki.save

puts "Added #{counter} cards."
